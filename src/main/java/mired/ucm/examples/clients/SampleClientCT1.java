/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mired.ucm.examples.clients;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import mired.ucm.remote.client.SGClient;
import mired.ucm.remote.orders.RemoteSwitchOn;
import mired.ucm.remote.server.SGServer;
import mired.ucm.remote.server.RemoteSGServer;
import mired.ucm.simulator.GridlabException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import junit.framework.TestCase;
public class SampleClientCT1  implements SGClient, Serializable {
		private static final long serialVersionUID = 1L;
		Timer timer;

		public SampleClientCT1(final RemoteSGServer stub) {
			timer = new Timer("transformerCT1");
			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					try {
						stub.executeOrder(SampleClientCT1.this.getName(),
								new RemoteSwitchOn("Solar_11")); // Sends an
																	// order
																	// to the
																	// server
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						this.cancel();
					}
				}
			};
			timer.scheduleAtFixedRate(task, 0, 10000);
		}

		// Client task

		@Override
		public void serverStopped() throws RemoteException {
			if (timer != null) {
				timer.cancel();
			}
		}

		@Override
		public String getName() throws RemoteException {
			return "transformerCT1";
		}
	
}
